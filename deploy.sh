#!/bin/bash

# Update yum
yum -y update

# Add node's source repo
curl -sL https://rpm.nodesource.com/setup_15.x | bash -

# Install node
yum -y install nodejs

# Create a dedicated directory for the app
mkdir -p /var/app

# Get the app from S3
# wget https://aws-tc-largeobjects.s3-us-west-2.amazonaws.com/ILT-TF-100-TECESS-5/app/app.zip
wget https://gitlab.com/aws2301402/employee-app/-/raw/main/app.zip

# Extract it to the app directory
unzip app.zip -d /var/app
cd /var/app

# Install dependencies
npm install

# Start the app
npm start
